﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class PlayersName : Form
    {
        public PlayersName()
        {
            InitializeComponent();
        }
        //metoda koja kada se stisne play sprema imena koja su upisana u varijable u formi1
        //kako bi ih mogli iskoristiti u ispisu pobjednika i u zapisivanju rezultata
        private void btn_play_Click(object sender, EventArgs e)
        {
            Form1.PlayerNames(p1.Text, p2.Text);
            this.Close();
        }
    }
}
